# CloudSimSDN
CloudSimSDN: SDN extension of CloudSim project.

## Introduction

**CloudSimSDN** is to simulate utilization of hosts and networks, and response time of requests in SDN-enabled cloud data centers. **CloudSimSDN** is an add-on package to [CloudSim](http://www.cloudbus.org/cloudsim/). This repository contains the necessary files including dependencies that are required to run CloudSimSDN. This repo is integrated from CloudSim - https://github.com/Cloudslab/cloudsim/ and CloudSimSDN - https://github.com/fogony/cloudsimsdn. When followed the steps as shown here and fulfilled the dependencies, the project is good to run.

## Program dependencies
* [CloudSim (cloudsim-3.0.3)](https://code.google.com/p/cloudsim/)
* [Apache Commons Math](http://commons.apache.org/proper/commons-math/download_math.cgi)
* [JSON Simple (json-simple-1.1.1)](https://code.google.com/p/json-simple/)
* [Google Guava (guava-17.0)](https://code.google.com/p/guava-libraries/)

## Simulation execution
To execute the example, use the following command:
```
java -cp jars/*:. examples/org/cloudbus/cloudsim/examples/CloudSimExample[1..8]
```
### EXAMPLE:
```
java -cp jars/*:. examples/org/cloudbus/cloudsim/examples/CloudSimExample1
```

To execute SDN simulation example, use the following command:
```
java -cp jars/*:. examples/org/cloudbus/cloudsim/sdn/example/SDNExample <LFF|MFF> [physical.json] [virtual.json] [workload1.csv] [workload2.csv] [...]
```

* ```<LFF | MFF>```: Choose VM placement policy. LFF(Least Full First) or MFF(Most Full First)
* ```[physical.json]```: Filename of physical topology (data center configuration)
* ```[virtual.json]```: Filename of virtual topology (VM creation and network request)
* ```[workload1.csv] ...```: Filenames of workload files. Multiple files can be supplied.

### EXAMPLE:
```
java -cp jars/*:. examples/org/cloudbus/cloudsim/sdn/example/SDNExample MFF dataset-energy/energy-physical.json dataset-energy/energy-virtual.json dataset-energy/energy-workload.csv > results.out
```
